<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

 use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{

//    #[Route('/', name: 'app_default')]


// UTILIZANDO ANTES DO SYMFONY 8 -  MAS VOU UTILIZAR PARA ACOMPANHAR O CURSO
    /**
     * @Route("/")
     */
    public function index(){

        $name = 'Abmael Ferreira';
        return $this->render('index.html.twig', /*Forma compacta*/compact('name')
            // forma de variavel associativa extensa ['name'=> $name]
        );
    }

    /**
     * @Route("/product/{slug}", name="product")
     */
    public function product($slug){

        return $this->render('single.html.twig', /*Forma compacta*/compact('slug')
        // forma de variavel associativa extensa ['name'=> $name]
        );
    }
}
